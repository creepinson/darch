package hooks

import (
	"fmt"

	"git.flowtr.dev/theoparis/darch/pkg/hooks"
	"github.com/urfave/cli/v2"
)

var listCommand = cli.Command{
	Name:  "list",
	Usage: "list hooks",
	Action: func(clicontext *cli.Context) error {

		hooks, err := hooks.GetHooks()
		if err != nil {
			return err
		}

		for _, hook := range hooks {
			fmt.Println(hook.Name)
		}

		return nil
	},
}

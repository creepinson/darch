package recipes

import (
	"fmt"
	"log"

	"git.flowtr.dev/theoparis/darch/pkg/recipes"
	"git.flowtr.dev/theoparis/darch/pkg/utils"
	"github.com/urfave/cli/v2"
)

var parentsCommand = cli.Command{
	Name:  "parents",
	Usage: "list all the parents of a recipe",
	Flags: []cli.Flag{
		&cli.BoolFlag{
			Name: "exclude-external",
		},
		&cli.BoolFlag{
			Name: "reverse",
		},
	},
	Action: func(clicontext *cli.Context) error {
		var (
			recipeName      = clicontext.Args().First()
			excludeExternal = clicontext.Bool("exclude-external")
			reverse         = clicontext.Bool("reverse")
		)

		if len(recipeName) == 0 {
			return fmt.Errorf("you must provide a recipe name")
		}

		rs, err := recipes.GetAllRecipes(getRecipesDir(clicontext))
		if err != nil {
			return err
		}

		current, ok := rs[recipeName]
		if !ok {
			return fmt.Errorf("recipe %s doesn't exist", recipeName)
		}

		results := make([]string, 0)

		finished := false
		for !finished {
			if current.InheritsExternal {
				if !excludeExternal {
					results = append(results, current.Inherits)
				}
				finished = true
			} else {
				current = rs[current.Inherits]
				results = append(results, current.Name)
			}
		}

		if reverse {
			results = utils.Reverse(results)
		}

		for _, result := range results {
			log.Println(result)
		}

		return nil
	},
}

package recipes

import (
	"fmt"
	"log"
	"sort"

	"git.flowtr.dev/theoparis/darch/pkg/recipes"
	"github.com/urfave/cli/v2"
)

var childrenCommand = cli.Command{
	Name:  "children",
	Usage: "list all the children for a recipe",
	Flags: []cli.Flag{
		&cli.BoolFlag{
			Name: "reverse",
		},
	},
	Action: func(clicontext *cli.Context) error {
		var (
			recipeName = clicontext.Args().First()
			reverse    = clicontext.Bool("reverse")
		)

		if len(recipeName) == 0 {
			return fmt.Errorf("you must provide a recipe name")
		}

		rs, err := recipes.GetAllRecipes(getRecipesDir(clicontext))
		if err != nil {
			return err
		}

		current, ok := rs[recipeName]
		if !ok {
			return fmt.Errorf("recipe %s doesn't exist", recipeName)
		}

		results := make([]string, 0)

		for _, r := range rs {
			if r.Inherits == current.Name {
				results = append(results, r.Name)
			}
		}

		if reverse {
			sort.Sort(sort.Reverse(sort.StringSlice(results)))
		}

		for _, result := range results {
			log.Println(result)
		}

		return nil
	},
}

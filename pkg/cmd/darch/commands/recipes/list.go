package recipes

import (
	"fmt"

	"git.flowtr.dev/theoparis/darch/pkg/recipes"
	"github.com/urfave/cli/v2"
)

var listCommand = cli.Command{
	Name:  "list",
	Usage: "list all recipes",
	Action: func(clicontext *cli.Context) error {
		rs, err := recipes.GetAllRecipes(getRecipesDir(clicontext))
		if err != nil {
			return err
		}

		for _, r := range rs {
			fmt.Println(r.Name)
		}

		return nil
	},
}

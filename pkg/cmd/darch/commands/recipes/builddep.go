package recipes

import (
	"fmt"

	"git.flowtr.dev/theoparis/darch/pkg/recipes"
	"git.flowtr.dev/theoparis/darch/pkg/utils"
	"github.com/urfave/cli/v2"
)

var builddepCommand = cli.Command{
	Name:      "build-dep",
	Usage:     "list dependencies for the given recipes",
	ArgsUsage: "<recipes>*N",
	Action: func(clicontext *cli.Context) error {
		var (
			recipeNamesOg = clicontext.Args()
		)

		allRecipes, err := recipes.GetAllRecipes(getRecipesDir(clicontext))
		if err != nil {
			return err
		}

		recipeNames := make([]string, 0)
		for i := 0; i < recipeNamesOg.Len(); i++ {
			recipeName := recipeNamesOg.Get(i)
			if _, ok := allRecipes[recipeName]; !ok {
				return fmt.Errorf("recipe %s doesn't exist", recipeName)
			}
			recipeNames = append(recipeNames, recipeName)
		}

		if len(recipeNames) == 0 {
			// We want dependencies for all images.
			for _, recipe := range allRecipes {
				recipeNames = append(recipeNames, recipe.Name)
			}
		}

		// First, let's make sure all the recipes we are building exist.
		for _, recipeName := range recipeNames {
			if _, ok := allRecipes[recipeName]; !ok {
				return fmt.Errorf("recipe %s doesn't exist", recipeName)
			}
		}

		dependencies := make([]string, 0)

		for _, r := range allRecipes {
			if utils.Contains(recipeNames, r.Name) {
				parents := walkRecipeRecursively(r, allRecipes)
				parents = utils.Reverse(parents)
				dependencies = append(dependencies, parents...)
			}
		}

		dependencies = utils.RemoveDuplicates(dependencies)

		for _, dependency := range dependencies {
			fmt.Println(dependency)
		}

		return err
	},
}

func walkRecipeRecursively(r recipes.Recipe, rs map[string]recipes.Recipe) []string {
	result := make([]string, 0)
	result = append(result, r.Name)
	if !r.InheritsExternal {
		children := walkRecipeRecursively(rs[r.Inherits], rs)
		result = append(result, children...)
	}
	return result
}

package grub

import (
	"fmt"
	"os"

	"git.flowtr.dev/theoparis/darch/pkg/cmd/darch/commands"
	"git.flowtr.dev/theoparis/darch/pkg/reference"
	"git.flowtr.dev/theoparis/darch/pkg/staging"
	"github.com/urfave/cli/v2"
)

var grubMenuEntryCommand = cli.Command{
	Name:        "menu-entry",
	Description: "output a menu entry for a staged item",
	ArgsUsage:   "<image[:tag]>",
	Action: func(clicontext *cli.Context) error {
		var (
			imageName = clicontext.Args().First()
		)

		err := commands.CheckForRoot()
		if err != nil {
			return err
		}

		imageRef, err := reference.ParseImage(imageName)
		if err != nil {
			return err
		}

		session, err := staging.NewSession()
		if err != nil {
			return err
		}

		stagedImages, err := session.GetAllStaged()
		if err != nil {
			return err
		}

		for _, stagedImage := range stagedImages {
			if stagedImage.Ref.FullName() == imageRef.FullName() {
				return session.PrintGrubMenuEntry(stagedImage, os.Stdout)
			}
		}

		return fmt.Errorf("image not found")
	},
}

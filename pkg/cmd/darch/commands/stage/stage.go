package stage

import (
	"git.flowtr.dev/theoparis/darch/pkg/cmd/darch/commands/stage/grub"
	"github.com/urfave/cli/v2"
)

var (
	// Command is the cli command for managing content
	Command = cli.Command{
		Name:  "stage",
		Usage: "manage the stage",
		Subcommands: cli.Commands{
			&listCommand,
			&uploadCommand,
			&removeCommand,
			&tagCommand,
			&runHooksCommand,
			&cleanCommand,
			&syncBootloaderCommand,
			&currentCommand,
			&grub.Command,
		},
	}
)

package images

import (
	"github.com/urfave/cli/v2"
)

var (
	// Command is the cli command for managing content
	Command = cli.Command{
		Name:  "images",
		Usage: "manage images",
		Subcommands: cli.Commands{
			&pullCommand,
			&pushCommand,
			&listCommand,
			&tagCommand,
			&removeCommand,
		},
	}
)

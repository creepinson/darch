package images

import (
	"context"

	"git.flowtr.dev/theoparis/darch/pkg/reference"
	"git.flowtr.dev/theoparis/darch/pkg/repository"
	"github.com/urfave/cli/v2"
)

var removeCommand = cli.Command{
	Name:      "remove",
	Usage:     "remove an image",
	ArgsUsage: "<image[:tag]>",
	Action: func(clicontext *cli.Context) error {
		var (
			image = clicontext.Args().First()
		)

		ref, err := reference.ParseImage(image)
		if err != nil {
			return nil
		}

		repo, err := repository.NewSession(repository.DefaultContainerdSocketLocation)
		if err != nil {
			return err
		}
		defer repo.Close()

		err = repo.RemoveImage(context.Background(), ref.FullName())
		if err != nil {
			return err
		}
		return nil
	},
}

# Darch

Think "Dockerfiles", but for immutable, stateless, graphical (or not) environments, booted bare-metal.

## ZSH Completion

To enable autocompletion for darch, add the following to your ~/.zshrc:

```zsh
PROG=darch
_CLI_ZSH_AUTOCOMPLETE_HACK=1
source ~/.config/zsh/urfave.zsh
```

Next, download [urfave cli zsh autocomplete](https://github.com/urfave/cli/blob/master/autocomplete/zsh_autocomplete) and place it in ~/.config/zsh/urfave.zsh.
